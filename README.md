# .nomedia file

## Contents

- Summary
- Full Explaination
- - What is this repository for
- - How is .nomedia file useful

## Summary

Simple and Safe Download for .nomedia file
used for hiding media files from view
in Gallery Apps on Android

## Full Explaination

### What is this repository for

If you need to create a `.nomedia` file on Android,
you can't do so without using your computer
or installing a thid-party file manager that is likely to be shady.

This is why I made this repository:
So you can just download that file.
And because the repository is open-source, you can verify for yourself that the file is completely safe
(empty, as it's supposed to be)

### How is .nomedia file useful

the `.nomedia` file is useful on Android:
Place it in any directory to hide all of the media it contains (like images, audio and other media)
from view in gallery apps like Photo Gallery, Music Player and Video Player.
